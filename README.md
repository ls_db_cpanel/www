 ----------------------------------------------------------------------------------------------------------------------
             
                                            LS Database Control Panel                                                                                    
 ***********************************************************************************************************************
                                                www.LadySword.net
 ***********************************************************************************************************************
 
    ---->>> DEMO APP <<<----<br>
    Downlaod and install the Android version here:<br>
        http://www.ladysword.net/main2/index.php/portfolio/78-android/221-download-ls-database-control-panel
<br>
    Note: iOS demo app is not yet available. <br><br>
 INTRO<br>
 LS Database Control Panel is a code module designed to view the contents of a mobile app's databases.  

 DISCLAIMER - PLEASE READ<br>
 This app is still in development. The MAIN REASON I am making it public is because I need to share the code with others.    <br>
    
    ---->>> IMPORTANT <<<---- 
    This code is NOT ready for public use or distribution.  Therefore, use at your own risk, and please no complaints. 
    
 It's something I have created from scratch using jQuery Mobile 1.4.5.  I havn't finished it off yet as I have been very busy with work and life. But it wouldn't take much work to make it into a really nice starter piece that could be helpful to other programmers. <br>

 SUMMARY<br>
 This is a hybrid mobile app I created using... <br>
    ~ SQLite<br>
    ~ Cordova<br>
    ~ HTML5<br>
    ~ CSS3<br>
    ~ Javascript<br>
    ~ jQuery Mobile<br>
    ~ Cordova SQLite Extension (Litehelpers)
 It's purpose is to be a generic Database Control Panel starter code / add-on module for many hybrid mobile apps.<br>
 
 TOP FEATURE<br>
    * LIST ALL USERS - The best part of this app (I think so, anyway. ;-) ) <br>
        To view it...  <br>
           1. Tap USERS in the main menu <br>
           2. Tap ADD/EDIT USERS <br>
           3. Scroll down below the ADD USER form  <br>
           4. Tap LIST ALL USERS button <br>
           5. Tap a user to open a drop-down profile for that user (This was working before, but something got broke during a recent transition. I hope to fix this ASAP.) <br>

 OTHER FEATURES <br>
    * SHOWS CURRENT TABLES in your internal SQLite database.  <br>
    * CREATES DATABASE on first time use. It also pre-populates the database with starter data. <br>
    * ADD USER FORM is not yet working, but the frame for the form is here.  <br>
    * DEVELOP ONCE - DEPLOY TO iOS and ANDROD. This is "HYBRID CODE", therefore, you can code it one time - but deploy it to multiple platforms - such as iOS and Android!  <br>

 HOW TO USE THIS CODE<br>
    You can either...<br>
        ~~ Insert it Into Your App ~~<br>
            1. copy and paste the contents of the www folder in to a folder inside your hybrid mobile code folder.<br>
            2. create a link to the main starting file for the module:<br> 
                db_control_panel.htm<br>
        ~~ Use it as a Stand-Alone App ~~<br>
            1. Create a new app project using your favorite app deployment tools (I use Cordova)<br>
            2. Once you have your app running ok, replace the "www" folder in your new app with the "www" folder and it's contents contained in this repo.  <br>
            3. Customize the code to your heart's desire. <br>
    This app will not work in a browser. The Cordova SQLite Extension doesn't yet work with internet browsers. 
  <br><br>
  DEVELOPER MODE <br>     
    There are a few alert statements in this application, which are only there for debugging purposes. These can be used by the developer 
    to debug, or to know where the application is at when it is processing functions, etc.   Of course, these will need to be commented out before releasing your app to the public. 

 
 - Sandi
 <br><br>
Sandi Laufenberg-Deku<br>
Software Engineer<br>
LadySword Productions & Hylux Software<br>
Divisions of YWHW Is Our Rock, Inc.<br>
www.LadySword.net<br><br><br><br>
