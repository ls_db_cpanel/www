// JavaScript Document    WORKS AS OF 10-26-16 0554PM
/*
 *----------------------------------------------------------------------------------------------------------------------
 *      _              _       ____                       _   ____                _            _   _                 
 *     | |    __ _  __| |_   _/ ___|_      _____  _ __ __| | |  _ \ _ __ ___   __| |_   _  ___| |_(_) ___  _ __  ___ 
 *     | |   / _` |/ _` | | | \___ \ \ /\ / / _ \| '__/ _` | | |_) | '__/ _ \ / _` | | | |/ __| __| |/ _ \| '_ \/ __|
 *     | |__| (_| | (_| | |_| |___) \ V  V / (_) | | | (_| | |  __/| | | (_) | (_| | |_| | (__| |_| | (_) | | | \__ \
 *     |_____\__,_|\__,_|\__, |____/ \_/\_/ \___/|_|  \__,_| |_|   |_|  \___/ \__,_|\__,_|\___|\__|_|\___/|_| |_|___/
 *                       |___/                                                                                       
 *
 ***********************************************************************************************************************
 ***********************************************************************************************************************
------------------------------------------------------------------------------------------------------------------------
'
'   Programmer's Name:   Sandi Laufenberg, Software Developer
'                        LadySword Productions
'                        www.Ladysword.com
                         262-777-8788
'                        sandi@ladysword.biz
'   Date Page Started:   Jan 10 2016
    
'   PLEASE FEEL FREE TO CONTACT ME IF YOU HAVE ANY QUESTIONS OR
'       NEED HELP WITH THIS PROJECT IN THE FUTURE.
'   APP NAME:       My Home Hub
'
'   This file:      db_base_functions_church_app.js
'
'   Purpose:        -------------------------------
							HOW THIS WORKS
					-------------------------------
					Database is created if it does not already exist. 
					This database is the standard SQLite db, and different than the one I use for HomeHub, 
					which depends on the Cordova-sqlite-storage add-on. - sandi 2-26-17
					------------------------------- 
'                   
'
'   Resources:      There are a number of Documentation files available to assist the developer.
'                   There are notes, Word docs, Visio diagrams, and others.  Please feel free to contact 
                    Sandi if you need a copy.  I
'                   fully believe in making such materials available to future developers at any such
'                   time as they are requested, and with no hesitation.
'   Users:          It is estimated there will be over a million users of this app.
'
'   Revisions:
'       DATE        WHO DID THEM        WHAT WAS DONE
'       ----        ------------        -------------
'
'*--------------------------------------------------------------------------  -->  */


//<!-- main ET SQLITE scripts used in this example -->

/* TO-DO LIST:
	* FIX THE ERROR TRAPPING
		- AFTER AN executeSql statement, we should have...  
				nullHandler,errorHandler
			 Per https://developer.apple.com/library/content/documentation/iPhone/Conceptual/SafariJSDatabaseGuide/UsingtheJavascriptDatabase/UsingtheJavascriptDatabase.html
			 
			 However, many of the statements below have this reversed so it is like this:
			 	errorHandler,nullHandler
			 This needs fixing but run the app first before making changes cuz if I recall, I may have tried this briefly some 
			 	weeks ago, but it broke the code. not sure why. - Sandi 
		- Go to this page and figure out the correct way to implement ERRORHANDLER and NULLHANDLERS:
				https://developer.apple.com/library/content/documentation/iPhone/Conceptual/SafariJSDatabaseGuide/UsingtheJavascriptDatabase/UsingtheJavascriptDatabase.html
*/


/* 	-------------------------------
			HOW THIS WORKS
	-------------------------------
	Database is created if it does not already exist. 
	This database is the standard SQLite db, and different than the one I use for HomeHub, which depends on the 
	Cordova-sqlite-storage add-on. - sandi 2-26-17
	------------------------------- */


/* 	-------------------------------
				4.2
		Create Global Variables
	------------------------------- 
This event happens before the onBodyLoad function occurs */
var flagAddDefaultData = "NOT SET YET";
var globalProgressMarker = "NOT SET YET";
var globalCurrentHtmlPage = "NOT SET YET";
						 
var db;
var dbShortName = 'churchdb 1.2h';
var dbVersion = '1.1';
var dbDisplayName = 'ChurchAppDB 1.2h';
var dbMaxSize = 65535;

/* drop these test db's later.
churchdb 1.0   
churchdb 1.1  
churchdb 1.1a   churchdb 1.1b  churchdb 1.1c  churchdb 1.  churchdb 1.1e  churchdb 1.1f  churchdb 1.1g  churchdb 1.1h
churchdb 1.2     
HomeHubDB 2.0
HomeHubtest1DB2.0  (version numbers kept beng incremented)
	look also at bu copies of the add_user_1.1.htm file to determine other names used.

HomeHubtest6DB2.0
  
*/

// this  is called when an error happens in a transaction
function errorHandler(transaction, error) {
// // // function errorHandler(error) {
   
   // See Developer Notes Module for Description and Meanings of Error Codes. - Sandi 
    alert('ERROR. Code Failed here (at this globalProgressMarker): ' + globalProgressMarker + '. "Other non-database-related error."   Error.message: ' + error.message + '.  Error.code: ' + error.code + '.');
   
   // RESET THE globalProgressMarker
   globalProgressMarker = "RE-SET";
   
   // Handle errors here
    var we_think_this_error_is_fatal = true;
    if (we_think_this_error_is_fatal) return true;
    return false;

   
}



// this is called when a successful transaction happens
function successCallBack() {
   // // // alert("DEBUGGING: Transaction succeeded.");


}

function nullHandler(){
	// nothing here yet
}



/* 	-------------------------------
				400.15
	           onBodyLoad()
	-------------------------------
// called when the application loads 
	CALLED BY: Html body tag. */
function onBodyLoad_db_control_panel(){

    // This alert is used to make sure the application is loaded correctly
    // you can comment this out once you have the application working
     // // // alert("DEBUGGING: we are in the onBodyLoad() function. Marker 1550");
	

    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	//        	400.15.05
	//	Does Browser Support Databases?
	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	// not all mobile devices support databases  if it does not, the following alert will display indicating the device will not be able to run this application
    if (!window.openDatabase) {
         // not all mobile devices support databases  if it does not, the following alert will display
         // indicating the device will not be albe to run this application
          alert('Databases are not supported in this browser. Try Chrome or Safari. Marker 1500');
		 return;
    }
	 
	 
	 // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	  //        	400.15.10
	  //		  openDatabase()
	  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // this line tries to open the database  locally on the device
    // if it does not exist, it will create it and return a database object stored in variable db
     db = openDatabase(dbShortName, dbVersion, dbDisplayName, dbMaxSize);

      // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      //        CREATE TABLES IF NEEDED
      // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~     
      // this line will try to create the table User in the database just created/opened
       db.transaction(function(tx){
      
            // you can uncomment this next line if you want the User table to be empty each time the application runs
            // tx.executeSql( 'DROP TABLE User',nullHandler,nullHandler);
          
            // This next line actually creates the table User if it does not exist and sets up all the fields and their data types.
            // NOTES:  * The UserId column is an auto incrementing column which is useful if you want to pull back distinct rows
            // 				easily from the table.
			//		   * A description of each of the user id fields follows the executeSQL statement below.  
            // // // DELETE. OLD ONE BEFORE 10-26-16 AT 5:08PM: 
			// // //  tx.executeSql( 'CREATE TABLE IF NOT EXISTS User( u_device_autoid INTEGER NOT NULL PRIMARY KEY, u_central_db_autoid INTEGER,	u_device_conc_id TEXT, u_main_combined_id TEXT, u_church_id INTEGER NOT NULL, u_fname TEXT NOT NULL,u_lname TEXT NOT NULL,u_username TEXT NOT NULL,u_password TEXT NOT NULL,u_email TEXT,u_user_type TEXT NOT NULL,u_access_code TEXT,u_block TEXT, u_welfare_id TEXT,u_tithe_id TEXT,u_registerDate TEXT NOT NULL,u_user_status TEXT NOT NULL,u_admin_notes BLOB)',
            // // //               [],nullHandler,errorHandler);
			tx.executeSql( 'CREATE TABLE IF NOT EXISTS User( u_central_db_autoid INTEGER, u_device_autoid INTEGER NOT NULL PRIMARY KEY, u_device_conc_id TEXT, u_main_combined_id TEXT, u_church_id INTEGER NOT NULL, u_title_or_known_as TEXT, u_fname TEXT NOT NULL,u_lname TEXT NOT NULL,u_username TEXT NOT NULL,u_password TEXT NOT NULL,u_email TEXT, u_phone_num1 TEXT, u_phone_num2 TEXT, u_user_type TEXT NOT NULL,u_access_code TEXT,u_block TEXT, u_welfare_id TEXT,u_tithe_id TEXT, u_registerDate TEXT NOT NULL,u_user_status TEXT NOT NULL,u_admin_notes BLOB, u_public_2_whole_ch_network TEXT NOT NULL)',
                             [],nullHandler,errorHandler);

			
			/* USER ID FIELDS EXPLAINED:  
					u_central_db_id			This value is the primary key for the central dbase - but we also may be receiving records created on standalone 
												devices running the app locally. So we need several id fields.
					u_device_autoid			This value is the primary key for the local SQLite dbase on each user's device. It alone cannot be used as the 
												unique id since there will be duplicate values generated on other user's devices. 
					u_device_conc_id		The device will formulate this value by concatenating like so:   julian date +  "|" + u_device_autoid

					u_main_combined_id		This will be the main ID used across all parts of the app.  It is a concatenated value like so:    
													u_central_db_autoid  +  "|" + u_device_conc_id					*/
			
							  
			/*  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
					     ADD DEFAULT DATA
				~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
			// check if table is empty or not. If it is empty, then add default data to pre-populate the table. - sandi 
			tx.executeSql('SELECT * FROM User;', [],
					function(tx, result) {
					
						 // // // alert ("DEV NOTE a100: flagAddDefaultData = " + flagAddDefaultData);
						 // IF WE DO HAVE RECORDS FROM OUR SQL QUERY, THEN BUILD OUR DISPLAY
						 //  && MEANS (Logical AND) Returns true if both logical operands are true. Otherwise, returns false.
						 // // //  // // // alert ("about to do the IF TEST");
						 if (result != null && result.rows != null) {
							 // // // alert ('DEV NOTE a102.1: INSIDE IF TEST FOR IF RESULT IS NOT AN EMPTY RECORDSET. ' );
								
							if (result.rows.length == 0) {   // THEN THERE ARE NO RECORDS. THEREFORE. WE DO NEED TO PRE-POPULATE THE TABLE. 
							
								 // // // alert ('DEV NOTE a102.2: result.rows.length = ' + result.rows.length);
								 // // // alert ("DEV NOTE a102.5: there are zero records in the dbase");
								// THERE ARE ZERO RECORDS IN THE DATABASE. Therefore, we need to add the default pre-existing records.
								flagAddDefaultData = "YES";	
								 // // // alert ("DEV NOTE a105: flagAddDefaultData = " + flagAddDefaultData);
						 
							} else {						//  THERE ARE RECORDS IN THE DBASE. SO WE DO NOT NEED TO PRE-POPULATE THE TABLE 
							
								flagAddDefaultData = "NO";
								 // // // alert ("DEV NOTE a110: flagAddDefaultData = " + flagAddDefaultData);
						 
							}
							
						 } else {	// THEN THERE ARE NO RECORDS. THEREFORE. WE DO NEED TO PRE-POPULATE THE TABLE. 
					
							 // // // alert ("DEV NOTE a170: the dbase shows as NULL, SO PREPOPULATE  (CODE WORD:  MADISON)");
							// SET THE FLAG AS THERE ARE ZERO RECORDS IN THE DATABASE. Therefore, we need to add the default pre-existing records. 
							flagAddDefaultData = "YES";	
							 // // // alert ("DEV NOTE a172: flagAddDefaultData = " + flagAddDefaultData);
						 
						 }
						 
						 //  // // // alert ("DEV NOTE a180: DROPPED OUT OF FIRST IF TEST. NOW CHECKING FOR FLAG VALUE - IF YES, THEN POPULATE. ");
						 
						 if (flagAddDefaultData == "YES") {
						 
							tx.executeSql('INSERT INTO User(u_central_db_autoid,u_device_conc_id,u_main_combined_id,u_church_id,u_title_or_known_as,u_fname,u_lname,u_username,u_password,u_email,u_phone_num1,u_phone_num2,u_user_type,u_access_code,u_block,u_welfare_id,u_tithe_id,u_registerDate,u_user_status,u_admin_notes,u_public_2_whole_ch_network) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)',
								["1", "", "","3", "", "Faustina", "Ackom", "faustie", "springtime","","0249 37 79 09","","Church Member","10003-3-684","0","N/A","N/A","10/25/2016 9:57","Active","","NO"],nullHandler,errorHandler);	

									 // // // alert ("DEV NOTE a184: MADE IT PAST 1ST EXECUTESQL");	
								
							tx.executeSql('INSERT INTO User(u_central_db_autoid,u_device_conc_id,u_main_combined_id,u_church_id,u_title_or_known_as,u_fname,u_lname,u_username,u_password,u_email,u_phone_num1,u_phone_num2,u_user_type,u_access_code,u_block,u_welfare_id,u_tithe_id,u_registerDate,u_user_status,u_admin_notes,u_public_2_whole_ch_network) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)',
							["2", "", "","3", "Developer","Sandi", "Laufenberg-Deku", "sandi", "5i35e4xc", "sandi.truthluvver@gmail.com","","","Super Admin","00000000","0","N/A","N/A","10/18/2016 0:00","Active","","YES"],nullHandler,errorHandler);
								
							tx.executeSql('INSERT INTO User(u_central_db_autoid,u_device_conc_id,u_main_combined_id,u_church_id,u_title_or_known_as,u_fname,u_lname,u_username,u_password,u_email,u_phone_num1,u_phone_num2,u_user_type,u_access_code,u_block,u_welfare_id,u_tithe_id,u_registerDate,u_user_status,u_admin_notes,u_public_2_whole_ch_network) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)',
							["3", "", "","1", "Developer","Eric", "Deku Jr.", "Eric.Deku", "basketball", "eric.deku13@yahoo.com","023 28 19 085","WhatsApp:  020 158 8004","Super Admin","00000001","0","N/A","N/A","10/18/2016 0:00","Active","","YES"],nullHandler,errorHandler);

							tx.executeSql('INSERT INTO User(u_central_db_autoid,u_device_conc_id,u_main_combined_id,u_church_id,u_title_or_known_as,u_fname,u_lname,u_username,u_password,u_email,u_phone_num1,u_phone_num2,u_user_type,u_access_code,u_block,u_welfare_id,u_tithe_id,u_registerDate,u_user_status,u_admin_notes,u_public_2_whole_ch_network) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)',
							["4", "", "","1", "", "Adline","Osei", "Adline.Osei", "springtime", "","","","Church Member", "10001-3-678","0","N/A","N/A","10/18/2016 0:00","Active","","NO"],nullHandler,errorHandler);
								
							tx.executeSql('INSERT INTO User(u_central_db_autoid,u_device_conc_id,u_main_combined_id,u_church_id,u_title_or_known_as,u_fname,u_lname,u_username,u_password,u_email,u_phone_num1,u_phone_num2,u_user_type,u_access_code,u_block,u_welfare_id,u_tithe_id,u_registerDate,u_user_status,u_admin_notes,u_public_2_whole_ch_network) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)',
							["5", "", "","1", "", "Henry","Osei", "Henry.Osei", "springtime", "","","","Church Member","10001-3-679","0","N/A","N/A","10/18/2016 0:00","Active","","NO"],nullHandler,errorHandler);
								
							tx.executeSql('INSERT INTO User(u_central_db_autoid,u_device_conc_id,u_main_combined_id,u_church_id,u_title_or_known_as,u_fname,u_lname,u_username,u_password,u_email,u_phone_num1,u_phone_num2,u_user_type,u_access_code,u_block,u_welfare_id,u_tithe_id,u_registerDate,u_user_status,u_admin_notes,u_public_2_whole_ch_network) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)',
							["6", "", "","2", "", "Derek","Deku", "Derek.Deku", "springtime", "","054 135 6678","","Church Member","10002-3-680","0","N/A","N/A","10/18/2016 0:00","Active","","NO"],nullHandler,errorHandler);
								
							tx.executeSql('INSERT INTO User(u_central_db_autoid,u_device_conc_id,u_main_combined_id,u_church_id,u_title_or_known_as,u_fname,u_lname,u_username,u_password,u_email,u_phone_num1,u_phone_num2,u_user_type,u_access_code,u_block,u_welfare_id,u_tithe_id,u_registerDate,u_user_status,u_admin_notes,u_public_2_whole_ch_network) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)',
							["7", "", "","1", "Prophet","Isaac", "Yirenkyi", "isaac.yirenkyi", "springtime", "","","","Pastor","10001-7-681","0","N/A","N/A","6/3/2014 11:47","Active","","NO"],nullHandler,errorHandler);
								
							tx.executeSql('INSERT INTO User(u_central_db_autoid,u_device_conc_id,u_main_combined_id,u_church_id,u_title_or_known_as,u_fname,u_lname,u_username,u_password,u_email,u_phone_num1,u_phone_num2,u_user_type,u_access_code,u_block,u_welfare_id,u_tithe_id,u_registerDate,u_user_status,u_admin_notes,u_public_2_whole_ch_network) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)',
							["8", "", "","2", "Rev. Sam (Bishop)","Ishmael", "Sam", "rev.sam", "springtime", "psishmael@gmail.com", "027 7533151","","Pastor","10002-7-682","0","N/A","N/A","6/30/2014 14:47","Active","","NO"],nullHandler,errorHandler);
								
							tx.executeSql('INSERT INTO User(u_central_db_autoid,u_device_conc_id,u_main_combined_id,u_church_id,u_title_or_known_as,u_fname,u_lname,u_username,u_password,u_email,u_phone_num1,u_phone_num2,u_user_type,u_access_code,u_block,u_welfare_id,u_tithe_id,u_registerDate,u_user_status,u_admin_notes,u_public_2_whole_ch_network) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)',
							["9", "", "","3", "Evangelist","Eric", "Deku Sr.", "eric", "springtime", "eric.cudjoe.deku@gmail.com", "026-145-5159", "024-566-1859", "Evangelist", "10003-7-683","0","N/A","N/A","7/23/2014 8:43","Active","","NO"],nullHandler,errorHandler);
								
							tx.executeSql('INSERT INTO User(u_central_db_autoid,u_device_conc_id,u_main_combined_id,u_church_id,u_title_or_known_as,u_fname,u_lname,u_username,u_password,u_email,u_phone_num1,u_phone_num2,u_user_type,u_access_code,u_block,u_welfare_id,u_tithe_id,u_registerDate,u_user_status,u_admin_notes,u_public_2_whole_ch_network) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)',
							["10", "", "","1", "", "TestFname","TesterLastName","tester1","testpassword","tester@test.com","02704560211","034 522 5555","Church Member","10003-3-685","0","N/A","N/A","3/8/2016 18:08","Deleted","","NO"],nullHandler,errorHandler);

							tx.executeSql('INSERT INTO User(u_central_db_autoid,u_device_conc_id,u_main_combined_id,u_church_id,u_title_or_known_as,u_fname,u_lname,u_username,u_password,u_email,u_phone_num1,u_phone_num2,u_user_type,u_access_code,u_block,u_welfare_id,u_tithe_id,u_registerDate,u_user_status,u_admin_notes,u_public_2_whole_ch_network) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)',
							["11", "", "","3", "", "Deborah","Ackom","deb","springtime","","0570 37 04 31","","Church Member","10003-3-686","0","N/A","N/A","10/25/2016 11:00","Active","","NO"],nullHandler,errorHandler);


							
								 // // // alert ("DEV NOTE a185: MADE IT PAST 11TH AND LAST EXECUTESQL. NOW RE-SETTNG THE FLAG");
						 
							 // re-set the flag
							 flagAddDefaultData = "RE-SET a.";
							 // // //  // // // alert ("DEV NOTE a200: default values were inserted. flagAddDefaultData = " + flagAddDefaultData);
							 
						} else {
							// re-set the flag
							 flagAddDefaultData = "RE-SET b.";
							 // // //  // // // alert ("DEV NOTE a210: default values were not inserted. flagAddDefaultData = " + flagAddDefaultData);
							  // // // alert ("DEV NOTE a187 FLAG RE-SET");
						 
						}
						 globalProgressMarker = "a188.0";
						  // // // alert ("DEV NOTE a188.1: NOW DROPPED OUT OF IF TEST CHECKING FLAG STATUS.");
						 
						 
					},errorHandler);
			
			// // // // alert ("DEV NOTE a189: NOW PROCESSED NULL AND ERROR HANDLER AT A188, NOW PROCESSING ERRORHANDLER AND SUCCESSCALLBACK. THESE MAY BE IN WRONG ORDER!");
			
	   },errorHandler,nullHandler);
		
		//  // // // alert ("DEV NOTE a189.2: NOW PROCESSED ERRORHANDLER AND SUCCESSCALLBACK. NOW DONE WITH THAT. WHAT'S NEXT?");
		
		$('#dbName').html(dbDisplayName).enhanceWithin();


		/* ---------------------------------------------------
				SWITCH CASE - TELLS WHICH 
				QUERIES & FUNCTIONS TO RUN
		   ---------------------------------------------------
		   Each page which will query the dbase - or run other functions - should have a short script at the end of the HEAD tag.  It will look like:
				<script>
					// set the globalCurrentHtmlPage = to this page
					setCurrentHtmlPageTo_ViewAllUsersSA();
				</script>
		   That script will call a simple function which is unique for each page. It will set the value of globalCurrentHtmlPage 
				(see setCurrentHtmlPageTo_ViewAllUsersSA() ).  Then, when we go through the onBodyLoad function, we will know which 
				query to run as we hit this SWITCH CASE. 		*/
		switch (globalCurrentHtmlPage) {
			case "ViewAllUsersSA":
				//ListDBValues();
				ListDBValuesIndentedListview();
				//Populate Main Navbar
				functPopulateMainNavBar();
				//Populate Left Side Panel.
				functGenerate_dbObjectsLeftPanel();
				break;
			case "db_control_panel.htm":
				ListAll_SQLite_Tables_on_This_Device();
				//Populate Main Navbar
				functPopulateMainNavBar();
				//Populate Left Side Panel.
				functGenerate_dbObjectsLeftPanel();
				break;
			case "add_user_1_2_htm":
				//Populate Main Navbar
				functPopulateMainNavBar();
				//Populate Left Side Panel.
				functGenerate_dbObjectsLeftPanel();
				break;
			default:
				// do nothing
		}

		
}



			

/* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
				ListDBValues()
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
// list the values in the database to the screen using javascript to update the #lbUsers element
function ListDBValues() {

     // // // alert("debugging: now inside the ListDBValues() function. Marker 1596.")
	
	if (!window.openDatabase) {
       alert('Databases are not supported in this browser.');
      return;
    }

	// this line clears out any content in the #lbUsers element on the page so that the next few lines will show updated
	// content and not just keep repeating lines
	 $('#lbUsers').html('');

	// this next section will select all the content from the User table and then go through it row by row
	// appending the UserId  FirstName  LastName to the  #lbUsers element on the page
	db.transaction(function(transaction) {

		transaction.executeSql('SELECT * FROM User;', [],
			function(transaction, result) {
			  
				// IF WE DO HAVE RECORDS FROM OUR SQL QUERY, THEN BUILD OUR DISPLAY
				//  && MEANS (Logical AND) Returns true if both logical operands are true. Otherwise, returns false.
				if (result != null && result.rows != null) {
						// ----- STARTING TAG FOR LIST VIEW ------

						// ui-corner-all: Adds rounded corners to the element. ui-shadow: Adds an item shadow around the element. ui-overlay-shadow: Adds an 
						//	overlay shadow around
						var content =  '<div ui-corner-all ui-shadow ><ul id="users_listview" data-theme = "c" data-role="listview" data-theme="b" data-filter="true" data-filter-placeholder="Search users..." data-inset="true">';
						
						// ----- CONTENTS OF LIST VIEW -----------
						for (var i = 0; i < result.rows.length; i = i + 1) {
							  var row = result.rows.item(i);
							  content = content + '<li><a href="index.html">';
							  content = content + '<h2>' + row.u_fname + ' ' + row.u_lname + '</h2>';
							  content = content + '<p><strong>u: ' + row.u_username + '.  Email: ' + row.u_email + '.  ChurchID: ' + row.u_church_id + '.</strong></p>';
							  content = content + '<p>Access Code: ' + row.u_access_code + '.  User Type: ' + row.u_user_type + '.  Blocked: ' + row.u_block ;
							  content = content + '.  Welfare ID: ' + row.u_welfare_id + '.  Tithe ID: ' + row.u_tithe_id + '.  Registration Date: ' + row.u_registerDate  ;
							  content=content   + ' status  id: '+ row.u_user_status+'.</p>';
							  content = content + '<p class="ui-li-aside"><strong>User ID:  ' + row. u_device_autoid + '</strong></p>';
							  content = content + '</a></li>';
						  
						}  // END OF FOR NEXT LOOP 
						
						content = content + '</ul></div>';
						// --------- END OF CONTENTS OF LIST VIEW -------------
						
						// Use this line to put the new content into a certain place, like a span or a div. 
						$('#lbUsers').html(content).enhanceWithin();
						
				} else {
					
					 alert('There are no records to display. Marker dbf2070');

				} //  ~~~~~~~~~~~ END OF IF TEST - TESTING FOR NULL OR EMPTY RECORDSET 
				 
			
			},errorHandler);
	},errorHandler,nullHandler);

	return;

}





/* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
		   ListDBValuesIndentedListview()
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
// list the values in the database to the screen using javascript to update the #lbUsers element - (FORMERLY THIS FUNCTION WAS CALLED ListDBValuesAccordian())
function ListDBValuesIndentedListview() {

     // // // alert("FIRST BLAH BLAH: now inside the ListDBValues() function. Marker 3596.")
	
	if (!window.openDatabase) {
       alert('Databases are not supported in this browser. Marker 3597.');
      return;
    }

	// this line clears out any content in the #lbUsers element on the page so that the next few lines will show updated
	// content and not just keep repeating lines
	 $('#lbUsers').html('');

	// this next section will select all the content from the User table and then go through it row by row
	// appending the USER DATA to the  #lbUsers element on the page
	db.transaction(function(transaction) {

		transaction.executeSql('SELECT * FROM User ORDER BY u_fname;', [],
			function(transaction, result) {
			  
				// IF WE DO HAVE RECORDS FROM OUR SQL QUERY, THEN BUILD OUR DISPLAY
				//  && = (Logical AND) Returns true if both logical operands are true. Otherwise, returns false.
				if (result != null && result.rows != null) {
					
					
						// ----- STARTING TAG FOR LIST VIEW ------ 
						//		NOTE  The "collapsible-list-item-style" is defined with in-line css in the HEAD section of the page HTML file. 
						//      TROUBLE-SHOOTING - i had added this in to the code, but later removed it. maybe it needs to be there, but I don't think so: 
						//  			'<div id="autodividers-linkbar-container">'+
											
						/* note: the autodivider linkbar on the right side is difficult to get the linking to work. I tried it many ways and could not succeed. this might be
									 related to the fact I am using a kind of special "collapsible indented listview" cuz I think this has special javascript and styles that
									 might interfere with the mechanics of the autodivider. Therefore, leaving it out for now. - Sandi 11-9-16
							var content2 =  		'<div id="sorter">' +
													'<ul data-role="listview">' +
														'<li><span>A</span></li>' +
														'<li><span>B</span></li>' +
														'<li><span>C</span></li>' +
														'<li><span>D</span></li>' +
														'<li><span>E</span></li>' +
														'<li><span>F</span></li>' +
														'<li><span>G</span></li>' +
														'<li><span>H</span></li>' +
														'<li><span>I</span></li>' +
														'<li><span>J</span></li>' +
														'<li><span>K</span></li>' +
														'<li><span>L</span></li>' +
														'<li><span>M</span></li>' +
														'<li><span>N</span></li>' +
														'<li><span>O</span></li>' +
														'<li><span>P</span></li>' +
														'<li><span>Q</span></li>' +
														'<li><span>R</span></li>' +
														'<li><span>S</span></li>' +
														'<li><span>T</span></li>' +
														'<li><span>U</span></li>' +
														'<li><span>V</span></li>' +
														'<li><span>W</span></li>' +
														'<li><span>X</span></li>' +
														'<li><span>Y</span></li>' +
														'<li><span>Z</span></li>' +
													'</ul>' +
												'</div><!-- /sorter -->' +		*/
							var content2 = '<div id="INDENTED LISTVIEW" data-demo-css="#collapsible-list-item-style" data-theme="d" data-content-theme="c" data-mini="true" data-inset="true">' +
											   '<ul data-role="listview" id="sortedList" data-collapsed-icon="arrow-r" data-expanded-icon="arrow-d" data-theme="d" class="ui-listview-outer" data-autodividers="true" data-divider-theme="b" data-filter="true" data-filter-placeholder="Search..." data-inset="true">';

							
						// ----- CONTENTS OF INDENTED LIST VIEW -----------  shows "u_device_autoid"  
						for (var i = 0; i < result.rows.length; i = i + 1) {
							  var row = result.rows.item(i);
							  content2 = content2 + '<li data-role="collapsible" class="ui-nodisc-icon ui-alt-icon" data-collapsed-icon="carat-d" data-expanded-icon="carat-u" data-iconpos="right" data-shadow="false" data-corners="false">';
							  //content2 = content2 + '	<h4>' + row.u_fname + ' ' + row.u_lname + '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; (ID: ' + row.u_device_autoid + ')</h4>';
							  // NOTE: (1) Ui-link-aside is not working probably cuz this is a special "indented" listview. 
							  //		(2)  Also, everything inside the header needs to be wrapped in <H2>, or it won't show. Probably for the same reason. 
							  // // // content2 = content2 + '	<h2>' + row.u_fname + ' ' + row.u_lname + '</h2><p class="ui-li-aside">ID: ' + row.u_device_autoid + '</p>';
							  content2 = content2 + '	<h2>' + row.u_fname + ' ' + row.u_lname + '</h2>';
							  content2 = content2 + '	   <div class="ui-nodisc-icon ui-alt-icon">' +
															  '<table><tr><td width="95%">&nbsp;</td><td>' +
																'<a href="#" title="Edit" onclick="functPullInRecord(' + i + ');" class="ui-btn ui-shadow ui-corner-all ui-icon-edit ui-btn-icon-notext ui-btn-inline">Edit</a>' +
																'<a href="#" title="Save Changes" onclick="functSaveChanges(' + result.rows.item['row.u_device_autoid'] + ');" class="ui-btn ui-shadow ui-corner-all ui-icon-check ui-btn-icon-notext ui-btn-inline">Save</a>' +
																'<a href="#" title="Delete" onclick="functDeleteRecord(' + result.rows.item['row.u_device_autoid'] + ');" class="ui-btn ui-shadow ui-corner-all ui-icon-delete ui-btn-icon-notext ui-btn-inline">Delete</a>' +
															  '</td></tr></table>' +
														   '</div>';
							  content2 = content2 + '	   <table width="100%" data-role="table" id="movie-table-custom" data-mode="reflow" class="movie-list">';
							  content2 = content2 + '		 <thead>'; 
                			  content2 = content2 + '		   <tr>'; 
                  			  content2 = content2 + '		    	<th data-priority="1">Device ID</th>'; 
                  			  content2 = content2 + '		    	<th data-priority="2">Username</th>'; 
                  			  content2 = content2 + '		    	<th data-priority="3">Status</th>'; 
                  			  content2 = content2 + '		    	<th data-priority="4">Email</th>'; 
                  			  content2 = content2 + '		    	<th data-priority="5">Access Code</th>';
							  content2 = content2 + '		    	<th data-priority="6">Blocked</th>'; 
                  			  content2 = content2 + '		    	<th data-priority="7">Tithe ID</th>'; 
                  			  content2 = content2 + '		    	<th data-priority="8">Church ID</th>'; 
                  			  content2 = content2 + '		    	<th data-priority="9">Welfare ID</th>'; 
							  content2 = content2 + '		    	<th data-priority="10">User Type</th>'; 
                  			  content2 = content2 + '		    	<th data-priority="11">Reg. Date</th>'; 
                  			  content2 = content2 + '		   </tr>'; 
							  content2 = content2 + '		 </thead>'; 
							  content2 = content2 + '		 <tbody>';
							  content2 = content2 + '		  <tr>';
							  content2 = content2 + '			<td>&nbsp;&nbsp;' + row.u_device_autoid + '</td>'; 
							  content2 = content2 + '		    <td>&nbsp;&nbsp;' + row.u_username + '</td>'; 
							  content2 = content2 + ' 			<td>&nbsp;&nbsp;' + row.u_user_status + '</td>'; 
							  content2 = content2 + '			<td>&nbsp;&nbsp;' + row.u_email + '</td>'; 
							  content2 = content2 + '			<td>' + row.u_access_code + '</td>'; 
							  content2 = content2 + '			<td>&nbsp;&nbsp;' + row.u_block + '</td>'; 
							  content2 = content2 + ' 			<td>&nbsp;&nbsp;' + row.u_tithe_id + '</td>'; 
							  content2 = content2 + '			<td>&nbsp;&nbsp;' + row.u_church_id + '</td>'; 
							  content2 = content2 + ' 			<td>&nbsp;&nbsp;' + row.u_welfare_id + '</td>'; 
							  content2 = content2 + '			<td>&nbsp;&nbsp;' + row.u_user_type + '</td>'; 
							  content2 = content2 + ' 			<td>&nbsp;&nbsp;' + row.u_registerDate + '</td></tr>'; 
							  content2 = content2 + '		   </tbody>';
							  content2 = content2 + '		</table>' ;
							  content2 = content2 + '</li>' ; 						  
						}  // END OF FOR NEXT LOOP 
						
						content2 = content2 + '   </ul>' ;
						content2 = content2 + '</div><!--/INDENTED LISTVIEW div -->' ;
						content2 = content2 + '	 	<a href="#"><i class="fa fa-times"></i>Close</a>' ;
                        content2 = content2 + '     <a href="#"><i class="fa fa-retweet"></i>Share</a>' ;
                        content2 = content2 + '     <a href="#"><i class="fa fa-link"></i>More</a>' ;
                            
						// --------- END OF CONTENTS OF ACCORDIAN LIST VIEW -------------
						
						// Use this line to put the new content into a certain place, like a span or a div. 
						$('#lbUsers').html(content2).enhanceWithin();
						
				} else {
					
					 alert('There are no records to display. Marker dbf3070');

				} //  ~~~~~~~~~~~ END OF IF TEST - TESTING FOR NULL OR EMPTY RECORDSET 
				 
			
			},errorHandler);
	},errorHandler,nullHandler);

	return;

}



			

/* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
				functPullInRecord()
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ 
Pulls in the data for this record so that user can edit the record. 
CALLED FROM:  Each table data view 			*/
function functPullInRecord(i)
{
	alert("Inside functPullInRecord");
}


/* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
				functSaveChanges()
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ 
Pulls in the data for this record so that user can edit the record. 
CALLED FROM:  Each table data view 			*/
function functSaveChanges(device_autoid)
{
	alert("Inside functSaveChanges)");
}



/* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
				functDeleteRecord()
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ 
Pulls in the data for this record so that user can edit the record. 
CALLED FROM:  Each table data view 			*/
function functDeleteRecord(device_autoid)
{
	alert("Inside functDeleteRecord()");
}		
			

			
			
/* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	 ListAll_SQLite_Tables_on_This_Device()
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ 
List ALL the SQLite Tables currently existing on the device. We will send the results to the screen using javascript to update the #lbList_All_SQLite_Tables_Here element
CALLED FROM:  "db_control_panel.htm"		*/
function ListAll_SQLite_Tables_on_This_Device() {

     
	if (!window.openDatabase) {
      alert('Databases are not supported in this browser. Marker dbf1001');
      return;
    }

	// this line clears out any content in the html element (i.e. #lbUsers) on the page so that we can fill it with fresh content. 	
	 $('#lbList_All_SQLite_Tables_Here').html('');

	// this next section will select all the content from the sqlite_master table, a system table, and then go through
	// it row by row, appending data from each row to the  #lbList_All_SQLite_Tables_Here element listview on the page
	db.transaction(function(transaction) {

		// // // alert(' just inside trx function for function ListAll_SQLite_Tables_on_This_Device().   Marker dbf1010.');
	
		transaction.executeSql('SELECT * FROM sqlite_master where type="table";', [],
			function(transaction, result) {
			  
				
				// IF WE DO HAVE RECORDS FROM OUR SQL QUERY, THEN BUILD OUR DISPLAY
				//  && MEANS (Logical AND) Returns true if both logical operands are true. Otherwise, returns false.
				if (result != null && result.rows != null) {
						
						// // // alert(' just inside NULL IF TEST.   Marker dbf1015.');
						// ----- CONTENTS OF LIST VIEW -----------
						// ui-corner-all: Adds rounded corners to the element. ui-shadow: Adds an item shadow around the element. ui-overlay-shadow: Adds an 
						//	overlay shadow around
						// // // var content =  '<div ui-corner-all ui-shadow ><ul id="users_listview" data-theme = "c" data-role="listview" data-theme="b" data-inset="true">';
						
						
						globalProgressMarker = "Marker dbf1050.";
	
						// ----- STARTING TAG FOR LIST VIEW ------
						var content =  '<div id="autodividers-linkbar-container">'+
											'<ul data-role="listview" id="all_tables_listview" data-theme="c" data-autodividers="true" data-filter="true" data-filter-placeholder="Search tables..." data-inset="true">';
											
						// NOTE: FIELDNAMES in the sqlite_master table:  type, name, tbl_name, rootpage, sql 
						
						// ----- CONTENTS OF LIST VIEW -----------
						for (var i = 0; i < result.rows.length; i = i + 1) {
							  var row = result.rows.item(i);
							  
							  content = content + '<li><a href="#" data-ajax="false">';
							  content = content + 'name: ' + row.name + ' | tbl_name: ' + row.tbl_name;
							  content = content + '</a></li>';
								if (i == 0) {
									// the first rows may end up outputting an empty set due to user being able to save an empty records
									//	so make sure if you get an empty row, that you add validation to the data entry for that table to 
									//  prevent empty records from being saved. 
								}
							 
						}  // END OF FOR NEXT LOOP 
						
						globalProgressMarker = "Marker dbf1060";
		
						content = content + '</ul><!-- /listview id="sortedList" -->';
						content = content + '</div> <!--  /id="autodividers-linkbar-container" -->';
						
						// --------- END OF CONTENTS OF LIST VIEW -------------
						// // // alert(' ready to insert contents. Contents equal...(Marker dbf1061).');
						
						// Use this line to put the new content into a certain place, like a span or a div. 
						$('#lbList_All_SQLite_Tables_Here').html(content).enhanceWithin();
						
				} else {
					
					alert('There are no records to display. Marker dbf1070');
				
				} //  ~~~~~~~~~~~ END OF IF TEST - TESTING FOR NULL OR EMPTY RECORDSET 
				 
			
			},errorHandler);
	},errorHandler,nullHandler);

	return;

}








/* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	 functGenerate_dbObjectsLeftPanel()
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ 
List ALL the SQLite Tables currently existing on the device. We will send the results to the screen using javascript to update the #lbList_All_SQLite_Tables_Here element
CALLED FROM:  "db_control_panel.htm"		*/
function functGenerate_dbObjectsLeftPanel() {

    if (!window.openDatabase) {
      alert('Databases are not supported in this browser. Marker dbf1101');
      return;
    }

	// this line clears out any content in the html element (i.e. #lbUsers) on the page so that we can fill it with fresh content. 	
	 $('#insertedHTML_dbObjectsLeftPanel').html('');
						// ----- STARTING TAG FOR LEFT PANEL ------
						var content =  '<a href="#" data-rel="close" data-position="right" class="ui-btn ui-icon-back ui-btn-icon-notext ui-corner-all">No text</a>' +
										'<p><img src="../../images/icons/home.png" class="ui-li-icon"><a href="../../main_menu.html">HomeHub Main Menu</a></p>' + 		'<p><h3>Objects</h3></p>' +
										'<p><a href="db_control_panel.htm"><img src="images/dbase/table_42x43.png"> Tables</a></p>' +
										'<p><img src="images/dbase/query_48x48.png"> Queries</p>' +
										'<p><img src="images/dbase/form_icon_48_tall.png"> Forms</p>' +
										'<p><img src="images/dbase/reports_40x44.png"> Reports</p>' +
										'<p><img src="images/dbase/pages_48_tall.png"> Pages</p>' +
										'<p>&nbsp;</p>' +	
										'<p><h3>Misc.</h3></p>' +
										'<p><img src="images/dbase/web-design_50_tall.png">Design Stuff</p>';
										
						// Use this line to put the new content into a certain place, like a span or a div. 
						$('#insertedHTML_dbObjectsLeftPanel').html(content).enhanceWithin();
	return;
}   // ----- END functGenerate_dbObjectsLeftPanel()




/* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	 functPopulateMainNavBar()
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ 
CALLED FROM:  "db_control_panel.htm"		*/
function functPopulateMainNavBar() {

    if (!window.openDatabase) {
      alert('Databases are not supported in this browser. Marker dbf1201');
      return;
    }

	// this line clears out any content in the html element (i.e. #lbUsers) on the page so that we can fill it with fresh content. 	
	 $('#insertedHTML_mainHeaderNavBar').html('');
						// ----- STARTING TAG ------
						var content =   '<!-- NOTE: alternatively, we could use the SELECT MENU to make the app act more like a native app. something to think about.' +
													'see "Select menu" in the JQM demos. - Sandi -->' +
										'<div data-role="navbar">' +
											'<ul>' +
												'<li><a href="#">Properties</a></li>' +
												'<li><a href="#">Items</a></li>' +
												'<!-- ---- USERS ---- -->' +
												'<li><div id="USERS Dropdown Navbar Item - Nested Listview" >' +
														'<div id="user-menu-item">' +
															'<a href="#USERS-dropdown-popup" data-rel="popup" data-transition="fade" class="my-tooltip-btn ui-alt-icon ui-mini" title="user-menu-item">' +
																	'Users</a>' +
														'</div>' +
													'</div><!-- /USERS Dropdown Navbar Item - Nested Listview --> </li>' +
												'<!-- ---- OTHER ---- -->' +
												'<li><div id="OTHER Dropdown Navbar Item - Nested Listview" >' +
														'<div id="other-menu-item">' +
															'<a href="#OTHER-dropdown-popup" data-rel="popup" data-transition="fade" class="my-tooltip-btn ui-alt-icon ui-mini" title="user-menu-item">' +
																	'Other</a>' +
														'</div>' +
													'</div><!-- /OTHER Dropdown Navbar Item - Nested Listview --> </li>' +
											'</ul>' +
									    '</div>' +
										'<!-- ---- USERS ---- -->' +
										'<div data-role="popup" id="USERS-dropdown-popup" data-theme="b">' +
											'<ul data-role="listview" data-inset="true" style="min-width:210px;">' +
												'<li data-role="list-divider">USERS Menu</li>' +
												'<li><a href="add_edit_user.htm">Add/Edit User</a></li>' +
												'<li><a href="view_all_users_super_admin.htm">User Table</a></li>' +
											'</ul>' +
										'</div>' +
										'<!-- ---- OTHER ---- -->' +
										'<div data-role="popup" id="OTHER-dropdown-popup" data-theme="b">' +
											'<ul data-role="listview" data-inset="true" data-mini="true" style="min-width:210px;">' +
												'<li data-role="list-divider">OTHER Menu</li>' +
												'<li><a href="http://www.homehubapp.com">Web Portal</a></li>' +
												'<li><a href="central_dbase_forwarding_url.htm">Central Dbase</a></li>' +
												'<li><a href="#db_control_panel.htm">DBase Control Panel</a></li>' +
											'</ul>' +
										'</div>';

										
						// Use this line to put the new content into a certain place, like a span or a div. 
						$('#insertedHTML_mainHeaderNavBar').html(content).enhanceWithin();
	return;
	
	/*
		<!-- Table of Contents (NavBar Dropdowns) -->
							<!-- ----- PROPERTIES ------ -->
							<!-- -------- ITEMS -------- -->
							<!-- -------- USERS -------- -->
							<!-- -------- OTHER ---- -->
							
						    <!-- NavBar DropDown Contents -->				
//							<!-- ----- PROPERTIES ------ -->
//							<!-- -------- ITEMS -------- -->
//							<!-- -------- USERS -------- 
									Add User
									View User Table
									Change User Status -->
							<!-- ------ OTHER ---- 
									* Web Portal
									* Central Dbase   -->
																	*/
	
}








/* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
			ListDBValues_UsersDirView() 
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
// list the values in the database to the screen using jquery to update the #lbUsers element






function ListDBValues_UsersDirView() {

	// // // // alert("debugging: now inside the ListDBValues_UsersDirView() function. Marker 1596b.")
	globalProgressMarker = "Marker 1596b";
						 
    if (!window.openDatabase) {
       alert('Databases are not supported in this browser.');
      return;
    }

	 // // // alert("The query ('strQuery1596') is filtered to only show members from a specific church. Originally this is set where u_church_id=1 (Eagles Church). To change this, search the code for the above query name, and change the WHERE statement in the query to a different church id. - Sandi.  ")
	
	// this line clears out any content in the #directory_main_listview element on the page so that the next few lines will show updated
	// content and not just keep repeating lines
	$('#directory_main_listview').html('');

	globalProgressMarker = "Marker 1596b1";
	
	var strQuery1596 = "SELECT [User].*, [User].u_user_status " + 
							"FROM [User] " +
							"WHERE (Not ([User].u_user_status)='Deleted')" +
							"AND (u_church_id=1)" +
							"ORDER BY u_fname;";

	// this next section will select all the content from the User table and then go through it row by row
	// appending the UserId  FirstName  LastName to the  #directory_main_listview element on the page
	db.transaction(function(transaction) {
		transaction.executeSql(strQuery1596, [],
			function(transaction, result) {
			 
				globalProgressMarker = "Marker 1596b2";
	
				// IF WE DO HAVE RECORDS FROM OUR SQL QUERY, THEN BUILD OUR DISPLAY
				//  && MEANS (Logical AND) Returns true if both logical operands are true. Otherwise, returns false.
				if (result != null && result.rows != null) {
					
					globalProgressMarker = "Marker 1596b3";
	
					// ----- STARTING TAG FOR LIST VIEW ------
					var content =  '<div id="autodividers-linkbar-container">'+
									'<div id="sorter">' +
										'<ul data-role="listview">' +
											'<li><span>A</span></li>' +
											'<li><span>B</span></li>' +
											'<li><span>C</span></li>' +
											'<li><span>D</span></li>' +
											'<li><span>E</span></li>' +
											'<li><span>F</span></li>' +
											'<li><span>G</span></li>' +
											'<li><span>H</span></li>' +
											'<li><span>I</span></li>' +
											'<li><span>J</span></li>' +
											'<li><span>K</span></li>' +
											'<li><span>L</span></li>' +
											'<li><span>M</span></li>' +
											'<li><span>N</span></li>' +
											'<li><span>O</span></li>' +
											'<li><span>P</span></li>' +
											'<li><span>Q</span></li>' +
											'<li><span>R</span></li>' +
											'<li><span>S</span></li>' +
											'<li><span>T</span></li>' +
											'<li><span>U</span></li>' +
											'<li><span>V</span></li>' +
											'<li><span>W</span></li>' +
											'<li><span>X</span></li>' +
											'<li><span>Y</span></li>' +
											'<li><span>Z</span></li>' +
										'</ul>' +
									'</div><!-- /sorter -->' +
										'<ul data-role="listview" data-theme="c" data-autodividers="true" id="sortedList" data-filter="true" data-filter-placeholder="Search users..." data-inset="true">';
										
					
				  // ----- CONTENTS OF LIST VIEW -----------
					for (var i = 0; i < result.rows.length; i = i + 1) {
						  var row = result.rows.item(i);
						  
						  content = content + '<li><a href="#" data-ajax="false">';
						  content = content + row.u_fname + ' ' + row.u_lname;
						  content = content + '</a></li>';
							if (i == 0) {
								// the first rows may end up outputting an empty set due to user being able to enter 
								// (sandi, i think this is supposed to be the 1st part of an IF TEST, with the ELSE
								//    being to form the content lines above.)
							}
						 
					}  // END OF FOR NEXT LOOP 
					
					globalProgressMarker = "Marker 1596.5a1";
	
					content = content + '</ul><!-- /listview id="sortedList" -->';
					content = content + '</div> <!--  /id="autodividers-linkbar-container" -->';
					
					// --------- END OF CONTENTS OF LIST VIEW -------------
					
					// ~~~~~~~~~~~~~~~~~~~
					//		HOW TO USE 
					// ~~~~~~~~~~~~~~~~~~~
					// Use this one to add the data at the end of the page:
					// // //   $( content ).appendTo( "#add_user_page1" ).enhanceWithin();
					
					// Use these two lines to put the new content into a certain place, like a span or a div. 
					$('#directory_main_listview').html(content).enhanceWithin();
					// ~~~~~~~~~~~~~~~~~~~
					
				
				}
			},errorHandler);
	},errorHandler,nullHandler);

	return;

}







// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//        ADD NEW RECORD
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// this is the function that puts values into the database using the values from the text boxes on the screen
function AddValueToDB() {

	// // // alert('addValueToDB 1');
	if (!window.openDatabase) {
	  alert('Databases are not supported in this browser.');
	 return;
	}
    // // // alert('addValueToDB 2');
	
    // this is the section that actually inserts the values into the User table ****
    db.transaction(function(tx) {
		globalProgressMarker = "dbf3010";
		tx.executeSql('INSERT INTO User(u_church_id, u_device_conc_id,u_fname,u_lname,u_username,u_password,u_user_type,u_registerDate,u_access_code,u_user_status) VALUES (?,?,?,?,?,?,?,?,?,?)',
							["1","FormulaNeedsCoding",$('#u_fname').val(), $('#u_lname').val(), $('#u_username').val(), $('#u_password').val(), $('#u_user_type').val(),  $('#u_registerDate').val(), $('#u_access_code').val(),  $('#u_user_status').val()],
							nullHandler,errorHandler);
	
    });
    // // // alert('addValueToDB 3');
	
    // this calls the function that will show what is in the User table in the database
    ListDBValues();
    // // // alert('addValueToDB 4');
	
     return false;

}
// ------------------  END ET SQLITE CODE -----------------



/* --------------------------------------------------------
					globalCurrentHtmlPage 
						 functions
   -------------------------------------------------------- */

   
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//      SET globalCurrentHtmlPage
//			to ViewAllUsersSA
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// set the globalCurrentHtmlPage = to the page which calls this function. 
function setCurrentHtmlPageTo_ViewAllUsersSA() {
	globalCurrentHtmlPage = "ViewAllUsersSA";
}


// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//      SET globalCurrentHtmlPage
//			to db_control_panel.htm
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// set the globalCurrentHtmlPage = to the page which calls this function. 
function setCurrentHtmlPageTo_db_control_panel_htm() {
	globalCurrentHtmlPage = "db_control_panel.htm";
}


// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//      SET globalCurrentHtmlPage
//			to add_user_1_2_htm
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// set the globalCurrentHtmlPage = to the page which calls this function. 
function setCurrentHtmlPageTo_add_user_1_2_htm() {
	globalCurrentHtmlPage = "add_user_1_2_htm";
}




// --------- END globalCurrentHtmlPage functions ---------- 




/* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
									TABLE FUNCTIONS
   ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */

   
   
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//      SET globalCurrentHtmlPage
//			to functLoadRecord
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// Load the record into the form on the screen so that it can be edited or just viewed.  
function functLoadRecord(i) 
{
 
    var item = dataset.item(i);
 
    $("#username").val((item['username']).toString());
 
    $("#useremail").val((item['useremail']).toString());
 
    $("#id").val((item['id']).toString());
 
}
























	/*
		CryptoJS Password Hashing Example found here:
			https://github.com/sytelus/CryptoJS
	*/
    var hash = CryptoJS.SHA3("Message");

    //The hash you get back isn't a string yet. It's a WordArray object.
    //When you use a WordArray object in a string context,
    //it's automatically converted to a hex string. 
     // // // alert(hash.toString()); //Same as hash.toString(CryptoJS.enc.Hex);




/*

	SOME OF THE MARKERS ADDED TO THIS PAGE:
	dbf1001 dbf1010 
	dbf1015 dbf1050 dbf1060 dbf 1061 dbf1070
	dbf1161 dbf1170
	dbf2070
	dbf3010
*/
